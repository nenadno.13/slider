$(document).ready(function() {

  $("#button-left").on("click", () => {

    let firstImageTopWidth = $("#slider-top-images img:first").width();
    let firstImageBottomWidth = $("#slider-bottom-images img:first").width();

    if ($(":animated").length) {
      return false;
    }

    $("#slider-top-images").css("left", firstImageTopWidth);
    $("#slider-top-images img:last").after($("#slider-top-images img:first"));
    $("#slider-top-images").animate({ left: 0 }, 1000);

    $("#slider-bottom-images").css("left", firstImageBottomWidth);
    $("#slider-bottom-images img:last").after($("#slider-bottom-images img:first"));
    $("#slider-bottom-images").animate({ left: 0 }, 1000); 
  
  });

  
  $("#button-right").on("click", function () {

    

    let marginWidth = 10;
    let firstImageTopWidth = $("#slider-top-images img:last").width() + marginWidth;
    let firstImageBottomWidth = $("#slider-bottom-images img:last").width() + marginWidth;

    if ($(":animated").length) {
      return false;
    }

    
    $("#slider-top-images").animate({ left: firstImageTopWidth }, 1000, function()  {
        $("#slider-top-images img:first").before($("#slider-top-images img:last"));
        $("#slider-top-images").css("left", "0px");
      });

   
    $("#slider-bottom-images").animate({ left: firstImageBottomWidth }, 1000, function(){
        $("#slider-bottom-images img:first").before($("#slider-bottom-images img:last"));
        $("#slider-bottom-images").css("left", "0px");
      });
  });
});
